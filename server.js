//REST server
var express = require('express');
var app = express();

//http server for files in filespace
var serveStatic = require('serve-static');

//filesystem for reading and writing files
var fs = require('fs');

//multipart http post request
var Busboy = require('busboy');

//utilities for writing files
var path = require('path');
var os = require('os');

//logging
var util = require('util');

//creating thumbnails
var thumb = require('node-thumbnail').thumb;


//parsing of rest requests
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());


//read beacon instances with properties from file
var locations = JSON.parse(fs.readFileSync('locations.json', 'utf8'));



//get available services
app.get('/getServices', function (req, res) {
	var paramId = req.param('id');
	util.log('/getServices ' + 'id: ' + paramId);

	var isValidId = false;

	//refresh locations from file
	locations = JSON.parse(fs.readFileSync('locations.json', 'utf8'));

	if (typeof paramId !== "undefined") {
		
		for(var i = 0; i < locations.length; i++){
        	var obj = locations[i];
			//util.log(obj['instance_id'] === id);
			if (obj['instance_id'].toLowerCase() === paramId.toLowerCase()) {
				isValidId = true;
				res.send(obj);
				return;
			}
    	}
		if (!isValidId) {
			var error = {"error":"Invalid instance ID"};
			res.send(error);
		}
	} else {
		var error = {"error":"No instance id given"}
		res.send(error);
	}
});


//get available services
//deprecated do not use
app.get('/getServicesDemo', function (req, res) {
	var paramId = req.param('id');
	util.log('/getServicesDemo ' + 'id: ' + paramId);

	var isValidId = false;

	if (typeof paramId !== "undefined") {
		for(var i = 0; i < locationsDemo.length; i++){
        	var obj = locationsDemo[i];
			if (obj['instance_id'] === paramId) {
				isValidId = true;
				res.send(obj);
				return;
			}
    	}
		if (!isValidId) {
			var error = {"error":"Invalid instance ID"};
			res.send(error);
		}
	}
});


// check if this instance id corresponds to a valid beacon region
function isValidInstanceId(id) {
	var isValid = false;
	for(var i = 0; i < locations.length; i++){
        var obj = locations[i];
		if (obj['instance_id'] === id) {
			isValid = true;
		}
    }
	return isValid;
}


//return the list of items in filespace from a specific beacon
app.get('/getFilesInSpace', function(req, res) {
	util.log("getFilesInSpace");

	var spaceId = req.param('id');

	if (typeof spaceId === 'undefined' || !isValidInstanceId(spaceId)) {
		res.end();
		return;
	}

	//purge expired files
	purgeExpiredFilesInSpace(spaceId);

	var path = "files/" + spaceId;

	var mkdirp = require('mkdirp');
	mkdirp(path, function(err) { 

		//create db.json if it does not exist
		if (!fs.existsSync(path + "/db.json")) {
			util.log("writing new db.json");
			fs.writeFileSync(path + "/db.json", JSON.stringify(JSON.parse("[]", null, 4)));
		}

		//better to keep list in some kind of database
		var obj = JSON.parse(fs.readFileSync('files/' + spaceId + "/db.json", 'utf8'));
		res.send(obj);
	});

});


//post an item to filespace without file attachement
app.post('/postComment', function(req, res) {

	util.log("postComment");

	var paramId = req.param('id');

	//util.log("req:" + JSON.stringify(req, null, 4));
	util.log(req.body.arguments + " " + paramId);

	var jsonArguments = JSON.parse(req.body.arguments);

	//Get the current list from file
	var obj = JSON.parse(fs.readFileSync('files/' + paramId + "/db.json", 'utf8'));

	//create object to be added
	var newFileObjectToSave = {
		"filename": "", 
		"comment" : jsonArguments.comment, 
		"user": jsonArguments.user, 
		"random_id": jsonArguments.random_id, 
		"timestamp": Date.now(),
		"expires": jsonArguments.expires,
		"account_id": jsonArguments.account_id};

	if (jsonArguments.hasOwnProperty("weblink")) {
		newFileObjectToSave.weblink= jsonArguments.weblink;
	}

	//util.log("new file: " + JSON.stringify(newFileObjectToSave, null, 4));

	//add new object
	obj.push(newFileObjectToSave);
	//util.log("all: " + JSON.stringify(obj, null, 4));
	//Fixme: Check duplicates?

	//save to file 
	fs.writeFileSync('files/' + paramId + '/db.json', JSON.stringify(obj, null, 4), 'utf-8'); 
	
	res.end();

});


//post an item to filespace without file attachement
app.post('/addUserSource', function(req, res) {

	util.log("post addUserSource");

	var paramId = req.param('id');

	util.log(req.body.information_service + " " + paramId);

	var jsonInformationService = JSON.parse(req.body.information_service);


	var path = "files/" + paramId;

	var mkdirp = require('mkdirp');
	mkdirp(path, function(err) { 

		//create user_sources.json if it does not exist
		if (!fs.existsSync(path + "/user_sources.json")) {
			util.log("writing new user_sources.json");
			fs.writeFileSync(path + "/user_sources.json", JSON.stringify(JSON.parse("[]", null, 4)));
		}

		//TODO better to keep list in some kind of database
		var obj = JSON.parse(fs.readFileSync(path + "/user_sources.json", 'utf8'));
		//res.send(obj);
		obj.push(jsonInformationService);

		fs.writeFileSync(path + "/user_sources.json", JSON.stringify(obj, null, 4));

		res.end();
	});


	/*
	//Get the current list from file
	var obj = JSON.parse(fs.readFileSync('files/' + paramId + "/db.json", 'utf8'));


	//add new object
	obj.push(newFileObjectToSave);
	//util.log("all: " + JSON.stringify(obj, null, 4));
	//Fixme: Check duplicates?

	//save to file 
	fs.writeFileSync('files/' + paramId + '/db.json', JSON.stringify(obj, null, 4), 'utf-8'); 
	*/

});


//return the list of items in filespace from a specific beacon
app.get('/getUserSources', function(req, res) {
	util.log("getUserSources");

	var spaceId = req.param('id');

	if (typeof spaceId === 'undefined' || !isValidInstanceId(spaceId)) {
		res.end();
		return;
	}


	var path = "files/" + spaceId;

	var mkdirp = require('mkdirp');
	mkdirp(path, function(err) { 

		//create db.json if it does not exist
		if (!fs.existsSync(path + "/user_sources.json")) {
			util.log("writing new db.json");
			fs.writeFileSync(path + "/user_sources.json", JSON.stringify(JSON.parse("[]", null, 4)));
		}

		//better to keep list in some kind of database
		var obj = JSON.parse(fs.readFileSync('files/' + spaceId + "/user_sources.json", 'utf8'));
		res.send(obj);
	});

});


app.get('/deleteUserSource', function(req, res) {
	util.log("deleteUserSource");
	

	var spaceId = req.param('id');
	var nameToDelete = req.param('name');

	util.log("deleting: " + spaceId + " " + nameToDelete);

	var path = "files/" + spaceId;

	//better to keep list in some kind of database
	var obj = JSON.parse(fs.readFileSync('files/' + spaceId + "/user_sources.json", 'utf8'));
	

	var found = false;
	for (var i in obj) {

		if (obj[i].hasOwnProperty(nameToDelete)) {
			//remove entry from array
			obj.splice(i, 1);
			found = true;
			break;
		}
	}

	//save update db.json to file
	if (found) {
		fs.writeFileSync(path + "/user_sources.json", JSON.stringify(obj, null, 4), 'utf-8'); 
	} else {
		util.log("Could not find user source to delete.");
	}

	//end http connection
	res.end();

});


//post item with attachement to file space
app.post('/uploadFile', function(req, res) {
	util.log("uploadFile");
	//util.log(req);
	var parmId = req.param('id');
	util.log("req:" + req);
	
    //res.send(user_id + ' ' + token + ' ' + geo);

	// Create an Busyboy instance passing the HTTP Request headers.
	var busboy = new Busboy({ headers: req.headers });
	
		
	busboy.on('field', function (fieldname, val) {
		util.log("field: " + fieldname + " " + val);
		//TODO check if valid id
		if (fieldname === "arguments") {

			//TODO check arguments for correctness
			var jsonArguments = JSON.parse(val);
			var id = jsonArguments.id;
			//var comment = jsonArguments.comment;
			//var myfilename = jsonArguments.filename;
			util.log("arguments: " + id + " " + jsonArguments.comment);

			// Listen for event when Busboy finds a file to stream.
			busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
				util.log("on file.")
	
				var saveTo = path.join(os.tmpDir(), path.basename(fieldname));
  				file.pipe(fs.createWriteStream("files/" + id + "/"  + filename));

				// We are streaming! Handle chunks
				file.on('data', function (data) {
					util.log("file on data.")
					// Here we can act on the data chunks streamed.
				});
		
				// Completed streaming the file.
				file.on('end', function () {
					util.log('Finished with ' + fieldname);
					var obj = JSON.parse(fs.readFileSync('files/' + id + "/db.json", 'utf8'));
					var newFileObjectToSave = {
						"filename": filename, 
						"comment" : jsonArguments.comment, 
						"user": jsonArguments.user, 
						"random_id": jsonArguments.random_id, 
						"timestamp": Date.now(), 
						"expires": jsonArguments.expires,
						"account_id": jsonArguments.account_id};
					var found = false;
					for (var i in obj) {
						if (obj[i].filename === filename) {
							obj[i] = newFileObjectToSave;
							found = true;
						}
					}
					if (!found) {
						obj.push(newFileObjectToSave);
					}
					fs.writeFileSync('files/' + id + '/db.json', JSON.stringify(obj, null, 4), 'utf-8'); 

					//create icon
					if (filename.endsWith(".jpg")) {
						var thumbPath = "files/" + id + "/" + filename + ".thumb"
						//create empty file for thumbnail
						fs.openSync(thumbPath, 'w');

						/*
						thumb({
							source: "files/" + id + "/"  + filename, // could be a filename: dest/path/image.jpg 
							destination: thumbPath,
							width: 128,
							overwrite: true,
							concurrency: 4
						}, function(err) {
								console.log('All done!');
						});
						*/

						var easyimg = require('easyimage');
						easyimg.rescrop({
   							src: "files/" + id + "/"  + filename, 
							dst: thumbPath,
  							width:500, 
							height:500,
  							cropwidth:128, cropheight:128,
   							x:0, y:0
						}).then(
							function(image) {
						    	console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
							},
							function (err) {
						    	console.log(err);
 							}
						);
					}
				});
			});
		}
	});
	
	// Listen for event when Busboy is finished parsing the form.
	busboy.on('finish', function () {
		util.log("busboy on finish");
		res.statusCode = 200;
		res.end();
	});
	
	// Pipe the HTTP Request into Busboy.
	req.pipe(busboy);

});


//delete a filespace element
app.get('/delete', function(req, res) {

	util.log("delete");
	//util.log(req);
	var paramId = req.param('id');
	var fileId = req.param('random_id');
	util.log("deleting: " + "instance_id: " + paramId + " random_id: " + fileId);

	var obj = JSON.parse(fs.readFileSync('files/' + paramId + "/db.json", 'utf8'));
	var found = false;
	for (var i in obj) {
		if (obj[i].random_id === fileId) {

			if (obj[i].filename) {
				//delete file from filesystem
				fs.unlink('files/' + paramId + "/" + obj[i].filename, function(err) {
					if (err) { 
						return util.log(err);
					}
					util.log("file deleted.");
				});	

				//delete thumbnail
				if (obj[i].filename.endsWith('.jpg')) {
					fs.unlink('files/' + paramId + "/" + obj[i].filename + ".thumb", function(err) {
						if (err) { 
							return util.log(err);
						}
						util.log("file deleted.");
					});	
				}
			
			}
			//remove entry from array
			obj.splice(i, 1);
			found = true;
			break;
		}
	}

	//save update db.json to file
	if (found) {
		fs.writeFileSync('files/' + paramId + '/db.json', JSON.stringify(obj, null, 4), 'utf-8'); 
	} else {
		util.log("Could not find file to delete.");
	}

	//end http connection
	res.end();

});


//Remove items from filespace if the expiry date has passed
//paramId = id of the relevant smart space
var purgeExpiredFilesInSpace = function(paramId) {
	//util.log("purgeExpiredFiles");
	//util.log(req);

	//read the list of items from db.json
	var obj = JSON.parse(fs.readFileSync('files/' + paramId + "/db.json", 'utf8'));

	//Date of now
	var dateNow = Date.now();
	var found = false;
	for (var i in obj) {
		//if expires == 0 => this file never expires
		if (obj[i].expires != 0 && obj[i].expires < dateNow) {
			//util.log(obj[i].expires + " " + dateNow);
			util.log("purging: " + JSON.stringify(obj[i], null, 4));

			//Delete attachement file from disk
			if (obj[i].filename) {
				//delete file from filesystem
				fs.unlink('files/' + paramId + "/" + obj[i].filename, function(err) {
					if (err) { 
						return util.log(err);
					}
					util.log("file deleted.");
				});	
				//delete thumbnail
				if (obj[i].filename.endsWith('.jpg')) {
					fs.unlink('files/' + paramId + "/" + obj[i].filename + ".thumb", function(err) {
						if (err) { 
							return util.log(err);
						}
						util.log("file deleted.");
					});	
				}
			}

			//remove entry from array
			obj.splice(i, 1);
			found = true;

		}
	}
	
	//If we found some old files that needed purging
	if (found) {
		fs.writeFileSync('files/' + paramId + '/db.json', JSON.stringify(obj, null, 4), 'utf-8'); 
		util.log("some files were purged.");
	} else {
		util.log("Nothing was purged.");
	}

}


//apk updater
var apkUpdater = require('apk-updater');
apkUpdater.enable(app, '/apkUpdate', 'files/apk_repo');



//file server to let users download attachements from file space
//app.use(serveStatic('files', {'index': ['default.html', 'default.htm']}))
app.use(express.static(__dirname + '/files'));

/*
var static = require('node-static');
 
// 
// Create a node-static server instance to serve the './public' folder 
// 
var file = new static.Server('./files');
 
require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        // 
        // Serve files! 
        // 
        file.serve(request, response);
    }).resume();
}).listen(8080);
*/


//main REST entry point
var server = app.listen(8081, function () {
	var host = server.address().address;
	var port = server.address().port;
	util.log("App is listening at http://%s:%s", host, port);

})
